# Drupal Commerce Cresh Payment Gateway
README file for Commerce Cresh Payment Module
Version 1.0

INTRODUCTION
------------
This project integrates Cresh online payments into
the Drupal Commerce payment and checkout systems according to Cresh API https://docs.cresh.eu/technical/modules/#prerequis

REQUIREMENTS
------------
This module requires the following:
* Drupal 9
* Drupal Commerce 2  (https://drupal.org/project/commerce)
	- Commerce Core
	- Commerce Payment (and its dependencies)
	- Commerce Cart


CONFIGURATION
-------------
* Create a new Cresh payment gateway.
	Administration >Commerce >Configuration >Payment gateways >Add payment gateway
	Cresh settings available:
	- Enable Cresh
	- Set Payment mode (Test or Live)
	- Enter API url, public key, private key, store id, cresh js lib url
		Use the API credentials provided by your Cresh merchant account.


HOW IT WORKS
------------
* General considerations:
	- The store owner must need a Cresh merchant account.
	- Customers should have a valid credit card.
		- Cresh provides dummy credit card numbers for testing:
			https://docs.cresh.eu/functional/tests/#cartes-de-credit-de-test

* Checkout workflow:
	It follows the Drupal Commerce Off Site checkout workflow.
	The customer should confirm his order and go to payment page,
	where he will be able to choose # of instalments,
	and then be redirected to Cresh payment gateway, where he will enter his credit card.

* Payment Terminal
	If transaction is 'success', customer will be redirected back to order confirmation page
	with success message. Depending on Drupal Checkout Flow Settings,
	order will be validated automatically or manually by store administrator.


TROUBLESHOOTING
---------------
* No troubleshooting pending for now.


TODO
----
* Add support for Drupal 8.0
* Add validation for private and secret keys, store id.

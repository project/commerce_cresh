(function ($, Drupal, drupalSettings) {
	'use strict';

	const API_KEY = drupalSettings.commerce_cresh.apiKey;
	const PAYMENT_API_URL = drupalSettings.commerce_cresh.apiUrl;
	const DEBUG = 'true';
	const cartTotal = drupalSettings.commerce_cresh.cartTotal;
	const formDataObj = JSON.parse(drupalSettings.commerce_cresh.payload);
	const payloadUpdateUrl = drupalSettings.commerce_cresh.updatePayload;
	function setup() {
		const cresh = Cresh({
			apiUrl: PAYMENT_API_URL,
			debug: DEBUG,
			displayOptions: {
				title: false,
				collapsible: false,
				selfSubmit: true,
			},
			publishableKey: API_KEY,
			payloadUpdateUrl
		})
		document.cresh = cresh
		const formDataObj = JSON.parse(drupalSettings.commerce_cresh.payload)

		cresh.checkout.setPayload(formDataObj)
		const totalInCents = Number(cartTotal)

		cresh.mount({
			amount: drupalSettings.commerce_cresh.order.total_amount, // in cents
			domElement: "cresh-pay",
		})
		// const creshPaymentOption = document.querySelector('input[data-module-name="creshpay"]')
		// listen to click on instalments 3x or 4x

		const instalmentX3Card = document.querySelector('li[data-instalments="3"]')
		const instalmentX4Card = document.querySelector('li[data-instalments="4"]')
		setTimeout(() => {
			const tosCheckbox = document.querySelector('#cresh-payment-form .tos-checkbox')
		}, "1 second")


		if(instalmentX3Card){
			instalmentX3Card.addEventListener('click', e => {
				// save instlment in prestashop local hidden input
				document.querySelector('input[name=selected_instalment]').value = '3'
				//changePrestashopSubmitButtonClass(cresh)
			})
		}
		if(instalmentX4Card){
			instalmentX4Card.addEventListener('click', e => {
				document.querySelector('input[name=selected_instalment]').value = '4'
				//changePrestashopSubmitButtonClass(cresh)
			})
		}


		// listenToPaymentOptionSelect()
	}

	Drupal.behaviors.CommerceCreshBehavior = {

		attach: function (context) {

		},
		detach: function (context, settings, trigger) {
			// Detaching on the wrong trigger will clear the Braintree form
			// on #ajax (after changing the address country, for example).
			if (trigger !== 'unload') {
				return;
			}
		}
	}

	document.addEventListener("DOMContentLoaded", () => {
		setup()
	});

})(jQuery, Drupal, drupalSettings);

<?php

namespace Drupal\commerce_cresh\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Drupal\rest\ResourceResponse;
use Drupal\rest\ResourceResponseInterface;
use Drupal\commerce_price\Price;

/**
 * Provides the Cresh payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "cresh",
 *   label = "Cresh",
 *   display_label = "Cresh",
 *   forms = {
      *     "offsite-payment" = "Drupal\commerce_cresh\PluginForm\OffsiteRedirect\CreshOffsitePaymentForm",
      *   },
 *
 *   js_library = "commerce_cresh/checkout",
 * )
 */

class OffsiteRedirect  extends OffsitePaymentGatewayBase  {

  /**
   * The order data.
   *
   * {@inheritdoc}
   */
  protected $order;

  /**
   * The order data.
   *
   * {@inheritdoc}
   */
  protected $orderTransation;
  /**
   * The order amount.
   *
   * {@inheritdoc}
   */
  protected $amount;
  /**
   * The refund amount.
   *
   * {@inheritdoc}
   */
  protected $refundAmount;
  /**
   * The order payment.
   *
   * {@inheritdoc}
   */
  protected $payment;

  /**
   * {@inheritdoc}
   */
  public function getSecurityKey() {
  	return $this->configuration['secret_key'];
  }

	/**
	 * {@inheritdoc}
   */
	public function getMerchantId() {
		return $this->configuration['store_id'];
	}

	/**
	 * {@inheritdoc}
   */
	public function defaultConfiguration() {
		return [
			'secret_key' => '',
			'api_url' => '',
			'public_key' => '',
			'store_id' => '',
			'cresh_url' => '',
		] + parent::defaultConfiguration();
	}

	/**
	 * {@inheritdoc}
   */
	public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
		$form = parent::buildConfigurationForm($form, $form_state);
		$form['api_url'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Payment API URL'),
			'#default_value' => $this->configuration['api_url'],
			'#required' => TRUE,
		];
		$form['public_key'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Publishable Key'),
			'#default_value' => $this->configuration['public_key'],
			'#required' => TRUE,
		];
		$form['secret_key'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Secret Key'),
			'#default_value' => $this->configuration['secret_key'],
			'#required' => TRUE,
		];
		$form['store_id'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Store ID'),
			'#default_value' => $this->configuration['store_id'],
			'#required' => TRUE,
		];
		$form['cresh_url'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Cresh JS URL'),
			'#default_value' => $this->configuration['cresh_url'],
			'#required' => TRUE,
		];
		$config = \Drupal::service('config.factory')->getEditable('commerce_cresh.settings');
    $config->set('store_id', $this->configuration['store_id'])->save();
    $config->set('secret_key', $this->configuration['secret_key'])->save();
		return $form;
	}

	/**
	 * {@inheritdoc}
   */
	public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
		parent::submitConfigurationForm($form, $form_state);
		if (!$form_state->getErrors()) {
			$values = $form_state->getValue($form['#parents']);
			$this->configuration['api_url'] = $values['api_url'];
      $this->configuration['public_key'] = $values['public_key'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['store_id'] = $values['store_id'];
      $this->configuration['cresh_url'] = $values['cresh_url'];
    }
  }

	/**
	 * {@inheritdoc}
   */
	public function getApiKey() {
		return $this->configuration['secret_key'];
  }

	/**
	 * {@inheritdoc}
   */
	public function getStoreId() {
  	return $this->configuration['store_id'];
	}

	/**
	 * {@inheritdoc}
   */
	public function getPublicKey() {
  	return $this->configuration['public_key'];
	}

	/**
	 * {@inheritdoc}
   */
	public function getApiURL() {
  	return $this->configuration['api_url'];
	}

	/**
	 * {@inheritdoc}
   */
	public function generateSeal(string $payload) {
		$secret = $this->getSecurityKey();
    $base64Encode = base64_encode(stripslashes($payload));
    $algorithm = 'sha512';
    $seal = hash_hmac(
    	$algorithm,
      $base64Encode,
      $secret
    );
    return $seal;
	}

	/**
	 * {@inheritdoc}
   */
	public function onReturn(OrderInterface $order, Request $request) {
  	$settings = [
			'secret_key' =>  $this->configuration['secret_key'],
      'store_id' => $this->configuration['store_id']
    ];
		if (isset($_GET['context']) && $_GET['context']) {
			$context = $_GET['context'];
      $seal = $_GET['seal'];
      $decodedContext = base64_decode($context);
      $sourceSeal = $this->generateSeal($decodedContext);
      $json = json_decode($decodedContext);
      if ($this->isPaymentValid($json, $sourceSeal, $order) !== TRUE) {
      	$this->messenger()->addMessage($this->t('Invalid Transaction. Please try again'), 'error');
        return $this->onCancel($order, $request);
      } else {
      	if ($json->responseCode !== 'success') {
        	$this->messenger()->addMessage($this->t('Invalid Transaction. Please try again'), 'error');
          return $this->onCancel($order, $request);
      	} else {
					$payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
					$payment = $payment_storage->create([
						'state' => 'completed',
						'amount' => new Price($json->amount, 'eur'),  /* @TODO What should we do when currency is not euro? */
						'payment_gateway' => $this->entityId,
						'order_id' => $json->orderId,
						'remote_id' => $json->transactionId,
						'remote_state' => $json->status
					]);
					$payment->save();
					$this->messenger()->addMessage(
						$this->t('Your Cresh payment was successful with Order id : @orderid and Transaction id : @payment_id',
							[
								'@orderid' => $json->orderId,
								'@payment_id' => $json->transactionId
							]
						));
				}
      }
    } else {
    	$this->messenger()->addMessage($this->t('Invalid Transaction. Please try again'), 'error');
    	return $this->onCancel($order, $request);
    }
	}

	/**
	 * {@inheritdoc}
   */
	public function onCancel(OrderInterface $order, Request $request) {
  	$this->messenger->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
    	'@gateway' => $this->getDisplayLabel(),
    ]));
    throw new PaymentGatewayException('Payment cancelled by user');
	}

	/**
	 * {@inheritdoc}
   */
	public function isPaymentValid($json, $sourceSeal, $order) {
  	if (!$json) {
    	$this->messenger()->addMessage($this->t('Cresh didnt respond with transaction'), 'error');
      return FALSE;
    }
		if (!$sourceSeal) {
			$this->messenger()->addMessage($this->t('Cresh didnt generate security code'), 'error');
				return FALSE;
		}
		if ($order->id() != $json->merchantReference) {
			$this->messenger()->addMessage(
				$this->t(' Order id and Merchant Reference are not the same. Order id : @orderid and merchant : @merchant',
					[
        		'@orderid' => $order->id,
          	'@merchant' => $json->merchantReference
        	]
      	), 'error');
      return FALSE;
		}
		$transaction_amount = (int)$json->amount;
		$order_amount = $order->getTotalPrice()->getNumber() * 100;
		if ($transaction_amount != (int)$order_amount) {
    	$this->messenger()->addMessage(
      	$this->t('Order total and transaction total are not the same. Transaction : @transactionamount and order : @orderamount',
        	[
          	'@transactionamount' => $transaction_amount,
            '@orderamount' => $order_amount
          ]
        ), 'error');
      return FALSE;
    }

    return TRUE;
  }
}

<?php

namespace Drupal\commerce_cresh\PluginForm\OffsiteRedirect;

use CommerceGuys\Addressing\Country\CountryRepository;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Utility\Html;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Symfony\Component\Mime\Header;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_tax\Entity\TaxType;
use Drupal\commerce_tax\TaxOrderProcessor;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_tax\StoreTaxInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack as RequestStack;


class CreshOffsitePaymentForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
	protected function getShippingProfile(OrderInterface $order) {
		if ($order->hasField('shipments')) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      foreach ($order->shipments->referencedEntities() as $shipment) {
      	return $shipment->getShippingProfile();
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function convertToCreshAddressArray(ProfileInterface $profile = NULL) {
  	if (empty($profile) || !$profile->hasField('address') || $profile->get('address')->isEmpty()) {
    	return [];
		}
		/** @var \Drupal\address\AddressInterface $address */
    $address = $profile->address->first();
    return [
    	'country' => $address->getCountryCode(),
      'zip' => $address->getPostalCode(),
      'street' => $address->getAddressLine1(),
		];
	}

  /**
   * Retrieves the language manager.
   *
   * @return \Drupal\Core\Language\LanguageManagerInterface
   *   The language manager.
   */
  public function getLanguageManager() {
    return \Drupal::service('language_manager');
  }

  /**
   * {@inheritdoc}
   */
	public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
  	$form = parent::buildConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
		$payment = $this->entity;
    $this->paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();

    $order = $payment->getOrder();
    $price = $payment->getAmount();
    $shipping_address = $this->getShippingProfile($order);
		if ($order->hasField('shipments') && !$order->get('shipments')->isEmpty()) {
    	$shipments = $order->get('shipments')->referencedEntities();
      $shipment = reset($shipments);
      $shipping_method = $shipment->getShippingMethod();
      $shipping_amount = (int)$shipment->getAmount()->getNumber() * 100;
    } else {
    	$shipping_amount = 0;
    }
    $order_id = $order->id();
    $order_items = $order->getItems();
    $total_amount = $order->getTotalPrice()->getNumber() * 100;
		$profiles = $order->collectProfiles();
		if (isset($profiles['shipping']) && $profiles['shipping']) {
    	$shipping_profile = $profiles['shipping']->get('address')->first();
    }
    if (isset($shipping_profile) && is_object($shipping_profile)) {
    	$shipping_country = $shipping_profile->country_code;
      if ($shipping_country == 'FR') {
      	$shipping_country_code = 'fra';
      } else {
      	$shipping_country_code = $shipping_country;
      }

      $shipping_address_array = [
      	'street_number' => '-',
        'address_line1' => $shipping_profile->address_line1,
        'city' => $shipping_profile->locality,
        'country' => $shipping_country_code,
        'zipcode' => $shipping_profile->postal_code,
      ];

    }

    $billing_profile = $profiles['billing']->get('address')->first();
    if (isset($billing_profile) && is_object($billing_profile)) {
			$billing_country = $billing_profile->country_code;

      if ($billing_country == 'FR') {
      	$billing_country_code = 'fra';
			} else {
      	$billing_country_code = $shipping_country;
			}

			$billing_address_array = [
      	'street_number' => '-',
        'address_line1' => $billing_profile->address_line1,
        'city' => $billing_profile->locality,
        'country' => $billing_country_code,
        'zipcode' => $billing_profile->postal_code,
      ];
    } else {
			$billing_address_array = $shipping_address_array;
		}

		if (isset($shipping_profile->given_name)) {
			$given_name = $shipping_profile->given_name;
		} else {
			$given_name = $billing_profile->given_name;
		}
		if (isset($shipping_profile->family_name)) {
			$given_name = $shipping_profile->family_name;
		} else {
			$given_name = $billing_profile->family_name;
		}

		if (!isset($shipping_address_array)) {
			$shipping_address_array = $billing_address_array;
		}

		$customer_array = [
    	'civility' => 'mr',
      'firstname' => $given_name,
      'lastname' => $given_name,
      'email' => $order->getEmail(),
      'phone' => NULL,
      'birthdate' => NULL
		];

		foreach ($order->getItems() as $order_item) {
    	$purchased_entity = $order_item->getPurchasedEntity();
      	if (!$purchased_entity || $purchased_entity->getEntityTypeId() != 'commerce_product_variation') {
        	continue;
			}
		}

		$items = [];
    foreach ($order_items as $order_item) {
    	$purchased_entity = $order_item->getPurchasedEntity();
			if ($purchased_entity->hasField('field_image')) {
      	$product_image_field = $purchased_entity->get('field_image')->getValue();
      } else if ($purchased_entity->hasField('field_images')) {
      	$product_image_field = $purchased_entity->get('field_images')->getValue();
      }
      if (isset($product_image_field) && $product_image_field) {
      	$file = File::load($product_image_field[0]['target_id']);
			}

			global $base_url;
			$base_url_parts = parse_url($base_url);
			$host = $base_url_parts['host'];

      $source = 'https://' . $host . '/product/' . $purchased_entity->id();

      if (isset($file) && $file) {
      	$product_img = $host . $file->createFileUrl();
      } else {
      	$product_img = $host . '/web/sites/default/files/default_images/star-wars-timeline-slice.jpeg'; /* @TODO replace blank image */
			}
      $product_quantity = (int)str_replace(',', '', $order_item->getQuantity());
			$store = $order->getStore();
      $adjustments = $order->collectAdjustments();
      $rate_amount = 0;
      $tax_amount = 0;
      if (isset($adjustments)) {
      	foreach ($adjustments as $adj) {
        	if ($adj->getType() == 'tax') {
          	$rate_amount += $adj->getAmount()->getNumber();
          }
        }
      }

			if (isset($rate_amount) && $rate_amount>1){
      	$tax_amount = new Price($rate_amount, $payment->getAmount()->getCurrencyCode());
      }

      if (isset($tax_amount) && $tax_amount !== 0) {
      	$tax_total = $tax_amount->getNumber() * 100;
      } else {
      	$tax_total = 0;
      }

      array_push($items, [
      	'label' => str_replace('"', "", $purchased_entity->getTitle()),
        'sku' => $purchased_entity->getSku(),
        'url' => $source,
        'image_url' => $product_img,
        'quantity' => $product_quantity,
        'unit_price' => (int)$order_item->getAdjustedUnitPrice()->getNumber() * 100,
        'currency' => strtolower($payment->getAmount()->getCurrencyCode()),
      ]);
		}
		$config = \Drupal::service('config.factory')->getEditable('commerce_cresh.settings');
		$config->set('return_url', $form['#return_url'])->save();
		$order_array = [
    	'callback_url' => $form['#return_url'],
      'currency' => strtolower($payment->getAmount()->getCurrencyCode()),  /* @TODO: Do we need currency exchange rate in case its not euro? */
      'customer' => $customer_array,
      'items' => $items,
      'shipping_amount' => (int)$shipping_amount,
      'billing_address' => $billing_address_array,
      'merchant_reference' => $order_id,
      'tax_amount' => (int)$tax_total,
      'total_amount' => (int)$total_amount,
      'store_id' => $this->paymentGatewayPlugin->getStoreId(),
      'shipping_address' => $shipping_address_array
    ];

		$decodedContext = json_encode($order_array, JSON_UNESCAPED_UNICODE);
    $sourceSeal = $this->paymentGatewayPlugin->generateSeal($decodedContext);
    $payload = [
    	'instalments' => 0,
      'order' => $order_array,
      'seal' => $sourceSeal,
    ];

		$url_options = [
			'absolute' => TRUE,
			'language' => $this->getLanguageManager()->getCurrentLanguage(),
		];
		$site_url = Url::fromRoute('<front>', [], $url_options)->toString();

		$form['#attached']['drupalSettings']['commerce_cresh'] = [
    	'sourceSeal' => $sourceSeal,
      'payload' => json_encode($payload),
      'order' => $order_array,
      'json_order' => json_encode($order_array),
      'apiKey' => $this->paymentGatewayPlugin->getPublicKey(),
      'apiUrl' => $this->paymentGatewayPlugin->getApiURL(),
      'cartTotal' => (int)$total_amount,
      'items' => $order_items,
      'updatePayload' => $site_url . '/commerce_cresh/payload-update',
		];

    $form['#id'] = Html::getId($form_state->getBuildInfo()['form_id']);
    $form['#attached']['library'][] = 'commerce_cresh/creshlib';
    $form['#attached']['library'][] = 'commerce_cresh/payment';
    $form['advanced'] = [
    	'#type' => 'container',
      	'#attributes' => [
       		'class' => ['entity-meta-test'],
          'id' => 'cresh-pay',
        ],
    ];

		if (empty($form['#return_url'])) {
    	throw new \InvalidArgumentException('The offsite-payment form requires the #return_url property.');
    }
    if (empty($form['#cancel_url'])) {
    	throw new \InvalidArgumentException('The offsite-payment form requires the #cancel_url property.');
    }
    if (!isset($form['#capture'])) {
    	$form['#capture'] = TRUE;
    }

    return $form;
	}

}
